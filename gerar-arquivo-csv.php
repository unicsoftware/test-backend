<?php

    //Definição de Header do arquivo para gerar o arquivo tipo CSV
    header('Content-type: application/csv');   
	header('Content-Disposition: attachment; filename=compras-do-ano.csv');   
	header('Content-Transfer-Encoding: binary');
    
    //recebe a lista dos produtos que estão como RETURN no arquivo .php
    $listaCompras = include('lista-de-compras.php');
    require('include\funcoes-genericas.php');

    //definir arquivo para escrita das informações juntamente com o cabeçalho do CSV
	$output = fopen('php://output', 'w');
	$array_header = [utf8_decode("Mês"), "Categoria", "Produto", "Quantidade"];
	fputcsv($output, $array_header, ';');

    //Ordena os meses levando em consideração a Chave
    uksort($listaCompras, "OrdenarMesChave");

    //faz a leitura do Array recuperado do arquivo {lista-de-compras.php}
	foreach($listaCompras as $k_mes => $v_mes){        //cada mês
        ksort($v_mes);
		foreach($v_mes as $k_categoria => $v_produto){   //cada item do produto
            arsort($v_produto);            
			foreach($v_produto as $k_nome => $v_qtde){        //gera propriedades para gravar no CSV

				$params = [
					'mes'           => trim($k_mes), 
					'categoria'     => trim(utf8_decode($k_categoria)), 
					'nome'          => utf8_decode(corrigePalavras(trim(($k_nome)))), 
					'quantidade'    => trim($v_qtde)
				];
				fputcsv($output, $params,';');
			}
		}
	}
	fclose($output);
