<?php

//----------------------------------
//  Corrigir Palavras
//----------------------------------
function corrigePalavras($palavra)
{
    switch ($palavra) {
        case "Papel Hignico":
            return ("Papel Higiênico");
            break;
        case "Brocolis":
            return ("Brócolis");
            break;
        case "Chocolate ao leit":
            return ("Chocolate ao leite");
            break;
        case "Sabao em po":
            return ("Sabão em pó");
            break;
        default:
            return $palavra;
    }
}

//----------------------------------
//    MEU ARRAY DE MESES
//----------------------------------
function arrayMeses(){
    return array(
        'janeiro','fevereiro', 'marco','março','abril','maio','junho','julho', 'agosto','setembro', 'outubro','novembro', 'dezembro'
    );
}

//----------------------------------
//    ORDENAR MESES
//----------------------------------
function OrdenarMesChave($a, $b){
    
    //Adaptei uma função onde ao receber os meses será validado pela ordem definida naturalmente com base no index do array abaixo       
    $sort = arrayMeses();

    if ($a == $b) {
        return 0;
    }
    return (array_search( strtolower($a),$sort) < array_search(strtolower($b), $sort)) ? -1: 1;
}