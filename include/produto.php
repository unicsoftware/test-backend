<?php

	class cl_produto_categoria extends clsBanco {
				
		private $_mes;
		private $_categoria;			
		private $_produto;			
		private $_quantidade;			
		
		function __construct (){					
				
				$this->_connect();				
					
		}
	
		
		public function gera_registro (  $mes
										,$indexMes
										,$categoria
										,$produto	
										,$quantidade) {	
			
			$id_categoria = $this->retorna_id ('id_categoria','tbl_akna_categoria',"nome_categoria = '".$categoria."'");
			if($id_categoria == ""){
				$sqlInsertCategoria = "INSERT INTO tbl_akna_categoria (nome_categoria) values ('".$categoria."')";
				$this->_query($sqlInsertCategoria);
				$id_categoria = $this->_insert_id();
			}

			$id_produto   = $this->retorna_id ('id_produto','tbl_akna_produto',"nome_produto = '".$produto."' and id_categoria = ".$id_categoria );
			if($id_produto == ""){
				$sqlInsertProduto = "INSERT INTO tbl_akna_produto (id_categoria, nome_produto) values (".$id_categoria.",'".$produto."')";
				$this->_query($sqlInsertProduto);
				$id_produto = $this->_insert_id();
			}
			
			$sqlInsertCompra = "INSERT INTO tbl_akna_compra (id_produto, index_mes, mes, quantidade) values (".$id_produto.",".$indexMes.",'".$mes."',".$quantidade.")";			
			$this->_query($sqlInsertCompra);					
			
		}	

		public function limpar_lista_compra () {				
					
			$sqlCompra = "DELETE FROM tbl_akna_compra";
			$this->_query($sqlCompra);		
			
		}	

		public function consultar_lista_compra(){
			$sqlCompra = "SELECT 	compra.mes, 
									categoria.nome_categoria, 
									produto.nome_produto, 
									compra.quantidade
							from 		tbl_akna_compra 	as compra
							inner join 	tbl_akna_produto 	as produto on compra.id_produto = produto.id_produto
							inner join 	tbl_akna_categoria 	as categoria on categoria.id_categoria = produto.id_categoria
							order by compra.index_mes, categoria.nome_categoria, compra.quantidade desc";
			$this->_query($sqlCompra);
		}
	}
?>