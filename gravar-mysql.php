<?php

	require("include\banco_mysqli.php");
	require("include\produto.php");
    require('include\funcoes-genericas.php');

    //recebe a lista dos produtos que estão como RETURN no arquivo .php
    $listaCompras = include('lista-de-compras.php');
    
	$obj = new cl_produto_categoria();
	$obj->limpar_lista_compra();

    //faz a leitura do Array recuperado do arquivo {lista-de-compras.php}
	foreach($listaCompras as $k_mes => $v_mes){        //cada mês
 		foreach($v_mes as $k_categoria => $v_produto){   
            arsort($v_produto);            
			foreach($v_produto as $k_nome => $v_qtde){        

				$obj->gera_registro(trim($k_mes),
									array_search( strtolower($k_mes),arrayMeses()) ,
									trim($k_categoria), 
									corrigePalavras(trim($k_nome)), 
									trim($v_qtde));
		
			}
		}
	}

	//printa na tela as informações gravados do banco
	$obj->consultar_lista_compra();					
	echo "<table border=1>
			<thead>
			<tr>
				<th>Meste</th>
				<th>Categoria</th>
				<th>Produto</th>
				<th>quantiadde</th>
			</tr>
			</thead>
			<tbody>";
	while($row = $obj->_fetch_array()){
		echo "<tr><td>{$row["mes"]}</td><td>{$row["nome_categoria"]}</td><td>{$row["nome_produto"]}</td><td>{$row["quantidade"]}</td></tr>";		
	}
	echo "</table>";
	unset($obj);
