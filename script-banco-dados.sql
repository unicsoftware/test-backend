DROP TABLE IF EXISTS `tbl_akna_categoria`;
create table tbl_akna_categoria (
	id_categoria int not null auto_increment,
	nome_categoria varchar(100),
    PRIMARY KEY (id_categoria)
);

DROP TABLE IF EXISTS `tbl_akna_produto`;
create table tbl_akna_produto (
	id_produto	int not null auto_increment,
    id_categoria int,    
	nome_produto varchar(100),
    PRIMARY KEY (id_produto)
);

DROP TABLE IF EXISTS `tbl_akna_compra`;
create table tbl_akna_compra (
	id_compra int not null auto_increment,
    id_produto int,
    index_mes int,
    mes	varchar(10),
	quantidade int,
    PRIMARY KEY (id_compra)
);

select * from tbl_akna_categoria;
select * from tbl_akna_produto ORDER BY nome_produto;
select * from tbl_akna_compra;



/*
QUERY PARA SIMULAR A CONSULTA FINAL
FIX UMA DAPTAÇÃO NO INDEX DO MES PARA QUE FICASSE EM ORDEM DO ARQUIVO CSV E DO 
REGISTRO GRAVADO NO ARRAY QUE ENVIARAM COMO MASSA DE DADOS, PORÉM A FORMA CORRETA DE GRAVAR UMA COMPRA É USANDO UM CAMPO DATA
*/
SELECT 	compra.mes, 
		categoria.nome_categoria, 
        produto.nome_produto, 
        compra.quantidade
from 		tbl_akna_compra 	as compra
inner join 	tbl_akna_produto 	as produto on compra.id_produto = produto.id_produto
inner join 	tbl_akna_categoria 	as categoria on categoria.id_categoria = produto.id_categoria
order by compra.index_mes, categoria.nome_categoria, compra.quantidade desc;